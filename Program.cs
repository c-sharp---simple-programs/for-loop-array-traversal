﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Planets
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] planets = { "Mercury", "Venus", "Earth", "Mars", "Jupiter", "Saturn", "Uranus" };

            for (int i = 0; i < planets.Length; i++)
            {
                Console.WriteLine(planets[i]);
            }

            Console.WriteLine("\nIn reverse:");

            for (int i = planets.Length - 1; i >= 0; i--)
            {
                Console.WriteLine(planets[i]);
            }

            Console.ReadKey();
        }
    }
}
